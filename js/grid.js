jQuery(function($) {
  $('.trailer-link').on('click', function(){
    let trailer = $(this)
      .parent().find('.trailer').text().trim();
    $('#trailer-modal').find('iframe').attr('src', trailer);
    $('#trailer-modal').modal();
  });
});
